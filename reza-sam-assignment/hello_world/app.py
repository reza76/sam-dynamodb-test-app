import json
import utils


# import requests
def lambda_handler(event, context):

    body = json.loads(event["body"])
    f_name = body['firstname']
    l_name = body['lastname']
    address = body['address']

    utils.InsertUser(f_name, l_name, address)
    return {
        "statusCode": 200,
        "body": json.dumps(
            "Successfully created"
            # "location": ip.text.replace("\n", "")
        ),
    }
