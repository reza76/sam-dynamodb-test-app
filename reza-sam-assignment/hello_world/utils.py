import boto3

client = boto3.client("dynamodb")
import os
import uuid


def InsertUser(f_name, l_name, address):
    response = client.put_item(
        TableName=os.environ["SamDynamoTable"],
        Item={
            "pk": {
                "S": uuid.uuid4().hex
            },
            "first_name": {
                "S": f_name
            },
            "last_name": {
                "S": l_name
            },
            "address": {
                "S": address
            }
        }
    )
    return response

